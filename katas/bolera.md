# Ejercicio TDD

## Objetivo

- Usando la metodología TDD
  - Elaborar pruebas suficientes para simular una partida de bolos de un usuario
    - Simular todo tipo de tiradas
    - Garantizar una cálculo de puntuación correcta al finalizar la partida
  - Elaborar el código para pasar las pruebas
  - Realizar técnicas de refactorización para dejar código sin:
    - Código muerto
    - Código repetido
  - Garantizar un coverage del 100%
