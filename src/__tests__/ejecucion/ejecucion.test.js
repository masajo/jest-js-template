describe('Ejemplos de ejecución única o salto de ejecución', () => {
  test.skip('Test saltado', () => {
    expect(true).toBeTruthy();
  });

  test.only('Test único en ejecución', () => {
    expect(true).toBeTruthy();
  });

  test('???', () => {
    expect(true).toBeTruthy();
  });

  test('Test 2 único en ejecución ', () => {
    console.log('Ejecuta este test???');
    expect(true).toBeTruthy();
  });

  describe.skip('Test suite saltada', () => {
    test('Test que no se debería ejecutar', () => {
      expect(true).toBeTruthy();
    });
  });
});
