import { suma } from '../calculadora'

// ? Test Suite Collection --> Describe con Describes
// ? Test Suite --> Describe con tests
// ? Test case -> Test

test('Debería sumar correctamente números enteros y positivos', () => {
  expect(suma(1, 2)).toBe(3) // verificación
});
