/**
 * Matchers más empleados en JEST
 */

import { edadAleatoria, nombreCompleto } from '../../utils/contacts';

describe('Matchers más comunes', () => {
  test('toBe', () => {
    expect(1 + 1).toBe(2);
  });

  test('Negación: .not', () => {
    expect(1 + 1).not.toBe(3);
  });

  test('toEqual', () => {
    expect(1 + 1).toEqual(2);
    // Comparativa con objetos
    const coche = { nombre: 'A3' };
    coche.marca = 'Audi';
    expect(coche).toEqual({ nombre: 'A3', marca: 'Audi' });
  })

  describe('Comparativas Null, undefined, false y 0', () => {
    test('Valores Null/undefined/falsy', () => {
      const valor = null;
      expect(valor).toBeNull(); // SI
      expect(valor).toBeDefined(); // SI
      expect(valor).not.toBeUndefined(); // SI
      expect(valor).toBeFalsy(); // SI
      expect(valor).not.toBeTruthy(); // SI
    });

    test('Valor 0', () => {
      const valor = 0;
      expect(valor).not.toBeNull(); // SI
      expect(valor).toBeDefined(); // SI
      expect(valor).not.toBeUndefined(); // SI
      expect(valor).toBeFalsy(); // SI
      expect(valor).not.toBeTruthy(); // SI
    });

    test('Valor boolean', () => {
      const valor = false;
      expect(valor).not.toBeNull(); // SI
      expect(valor).toBeDefined(); // SI
      expect(valor).not.toBeUndefined(); // SI
      expect(valor).toBeFalsy(); // SI
      expect(valor).not.toBeTruthy(); // SI
    });
  });

  describe('Números', () => {
    test('Mayor/ Menor qué', () => {
      const valor = 3 + 2;
      expect(valor).toBeGreaterThan(4);
      expect(valor).toBeGreaterThanOrEqual(5);
      expect(valor).toBeLessThan(6);
      expect(valor).toBeLessThanOrEqual(5);
    });

    test('Igualdad', () => {
      const valor = 3 + 2;
      expect(valor).toBe(5);
      expect(valor).toEqual(5);
    });

    test('Aproximaciones en números decimales', () => {
      const valor = 0.3 + 0.5;
      //   expect(valor).toBe(0.8);
      expect(valor).toBeCloseTo(0.8);
    })
  });

  describe('Cadenas de Texto', () => {
    test('Comparativas', () => {
      const nombre = 'Martín';
      expect(nombre).toBe('Martín');
      expect(nombre).toEqual('Martín');
      expect(nombre).toMatch('Martín');
      expect(nombre).toMatch(/Mar/); // Expresión regular
      expect(nombre).not.toMatch(/er/); // Expresión regular
    });
  });

  describe('Listas e Iterables', () => {
    test('Comparativas y Contenido', () => {
      const nombre = 'Martín';
      const listaCompra = ['pollo', 'lechuga', 'agua'];

      expect(listaCompra).toStrictEqual(['pollo', 'lechuga', 'agua']);
      expect(listaCompra).toEqual(['pollo', 'lechuga', 'agua']);

      expect(listaCompra).toContain('agua');

      const elementosEliminados = listaCompra.splice(2, 1);

      expect(listaCompra).not.toContain('agua');
      expect(elementosEliminados).toStrictEqual(['agua']);

      expect(nombre).toContain('a');

      expect(new Set(listaCompra)).toContain('lechuga');
    })
  });

  describe('Creación Matchers propios', () => {
    expect.extend({
      toBeWithinRange (value, start, end) {
        // criterio para pasar el test
        const pass = value >= start && value <= end;
        if (pass) {
          return {
            message: () => `Expected ${value} to be within ${start} - ${end}`,
            pass: true
          }
        } else {
          return {
            message: () => `${value} is not within ${start} - ${end}`,
            pass: false
          }
        }
      }
    });

    test('Ejemplo uso de expect personalizado', () => {
      const valor1 = 10;
      const valor2 = 20;

      expect(valor1).toBeWithinRange(5, 100);
      expect(valor2).not.toBeWithinRange(1, 5);

      const producto = {
        precio: 1000,
        precioReducido: 500
      }

      expect(producto).toEqual({
        precio: expect.toBeWithinRange(999, 2000),
        precioReducido: expect.toBeWithinRange(100, 500)
      });
    })
  });

  describe('Creación Matchers asíncronos propios', () => {
    expect.extend({
      async valorValido (value) {
        const valorObtenido = await fetch({ url: 'http://miurl/valor', method: 'get' });
        const pass = valorObtenido === value;

        if (pass) {
          return {
            message: () => `${value} equals ${valorObtenido}`,
            pass: true
          }
        } else {
          return {
            message: () => `${value} does not equals ${valorObtenido}`,
            pass: false
          }
        }
      }
    })

    test.skip('Valor igual a Valor Asíncrono', async () => {
      await expect(400).valorValido();
      await expect(500).not.valorValido();
    });
  });

  describe('Mocks de Jest Expect Anything & Any', () => {
    test('Any o Concretos', () => {
      // Mock de funciones

      // * Comprobar que una función es llamada con cualquier parámetro de un tipo
      const mock = jest.fn(); // Función Mock está controlada por Jest
      edadAleatoria(mock, true); // Llamamos al método que dentro ejecuta la función mock (callback)
      // Verificamos que la función "mock" se ha llamado con cualquier número
      expect(mock).toBeCalledWith(expect.any(Number));

      // * Comprobar que una función NO es llamada
      const mock2 = jest.fn();
      edadAleatoria(mock2, false); // Llamamos al método que dentro ejecuta la función mock (callback)
      // Verificamos que la función "mock" NO se ha llamado con cualquier número
      expect(mock2).not.toBeCalled();
    });
    test('Returns Anything', () => {
      const mock = jest.fn();
      const listaCompra = ['patatas', 'cererales', 'agua'];

      listaCompra.map(producto => mock(producto));
      // * Comprobar que un método es llamado con cualquier parámetro
      expect(mock).toBeCalledWith(expect.anything());
    });
  });

  describe.skip('Mock de funciones propias', () => {
    // * Comprobar que una función es llamada con parámetros concretos
    const nombre = 'Martín';
    const apellido = 'San José de Vicente';

    const contacto = {
      nombre,
      apellido
    }
  });
});
