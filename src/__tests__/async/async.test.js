/**
 * Ejemplos de tests para gestión de procesos asíncronos
 */

import { obtenerContactoAleatorio, obtenerPromesa } from '../../utils/contacts';

describe('Ejemplos de asincronía', () => {
  test.skip('Obtener resultado', () => {
    return obtenerContactoAleatorio()
      .then((response) => {
        expect(response).toBe(expect.anything())
      })
  });

  test.skip('Obtener resultado con error', () => {
    return obtenerContactoAleatorio()
      .catch((error) => {
        expect(error).toMatch(/Error:/)
      })
  });

  test.skip('resolve', () => {
    return expect(obtenerPromesa(true)).resolves.toBe('Valor correcto')
  });

  test.skip('reject', () => {
    return expect(obtenerPromesa(false)).rejects.toMatch(/Error:/)
  });
});
