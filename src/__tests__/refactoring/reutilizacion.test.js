describe('Ejemplos Before y After', () => {
  let contacto = null;

  beforeEach(() => {
    console.log('1. Función encargada de ejecutarse antes de cada test');
    contacto = {
      nombre: 'Martín',
      apellidos: 'San José'
    }
  });

  afterEach(() => {
    console.log('1. Función encargada de ejecutarse después de cada test');
  });

  beforeAll(() => {
    console.log('1. Función encargada de ejecutarse antes todos los test');
  });

  afterAll(() => {
    console.log('1. Función encargada de ejecutarse después de todos los test');
  });

  test('Contacto debería ser Martín', () => {
    console.log('1. * test');
    expect(contacto).toStrictEqual({
      nombre: 'Martín',
      apellidos: 'San José'
    })
  });

  describe('Test Suite Anidado', () => {
    beforeEach(() => {
      console.log('2. Función encargada de ejecutarse antes de cada test');
      contacto = {
        nombre: 'Pepe',
        apellidos: 'García'
      }
    });

    afterEach(() => {
      console.log('2. Función encargada de ejecutarse después de cada test');
    });

    beforeAll(() => {
      console.log('2. Función encargada de ejecutarse antes todos los test');
    });

    afterAll(() => {
      console.log('2. Función encargada de ejecutarse después de todos los test');
    });

    test('Contacto debería ser Pepe', () => {
      console.log('2. * test')
      expect(contacto).toStrictEqual({
        nombre: 'Pepe',
        apellidos: 'García'
      })
    });
  });
});

/**
 * Orden de ejecución según ámbito
 * - 1. beforeAll
 * - 1. beforeEach
 * - 1. * test
 * - 1. afterEach
 * - 2. beforeAll
 * - 1. beforeEach
 * - 2. beforeEach
 * - 2. * test
 * - 2. afterEach
 * - 1. afterEach
 * - 2. afterAll
 * - 1. afterAll
 */
