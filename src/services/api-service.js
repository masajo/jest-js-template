import ApiRequest from '../utils/axios.config'

/**
 * Función para autenticar usuarios en enpoint e nuestra APi Restful
 * @param {string} email
 * @param {string} password
 * @returns {Promise<any>} Promesa con la respuesta de la API Restful para login de usuarios
 */
export function authUser (email, password) {
  const body = {
    email: email,
    password: password
  }

  return ApiRequest.post('/login', body)
}
