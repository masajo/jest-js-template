import axios from 'axios';

export default axios.create(
  {
    baseURL: 'https://miApi.com/api',
    responseType: 'json'
    // Añadir configuraciones generales para las peticiones a la api restful
  }
)
