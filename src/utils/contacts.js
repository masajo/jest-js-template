import axios from 'axios';

/**
 *
 * @param {string} nombre
 * @param {string} apellido
 * @returns {string} Nombre completo del contacto
 */
export function nombreCompleto (nombre, apellido) {
  return `${nombre} ${apellido}`;
}

export function edadAleatoria (gestionar, ok) {
  if (ok) {
    gestionar(Math.floor(Math.random() * 30 + 10))
  }
}

export function mostrarDatosContacto (contacto) {
  console.log(nombreCompleto(contacto.nombre, contacto.apellido));
}

/**
 * Función para obtener usuarios aleatorios
 * @returns {Promise<AxiosResponse<any, any>>} un usuario aleatorio
 */
export function obtenerContactoAleatorio () {
  return axios.get('https://randomuser.me/api');
}

export function obtenerPromesa (ok) {
  // TODO: Devolver Promesa
}

/**
 * Ejemplo de uso de función que devuelve una Promesa

obtenerContactoAleatorio()
  .then((respuesta) => {
    console.table(respuesta)
  })
  .catch((error) => console.error(`Ha ocurrido un error: ${error}`))
  .finally(() => console.info('Petición terminada'))
*/
